import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { Flight } from './flight';
import { PageService } from './share/page.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit{

  flight : Flight;

  flightForm !: FormGroup;
  flightList: Array<Flight> = [];

  todayDate : Date = new Date();
  // tempDate : any;

  NewDate : any;

  CheckCountry = false;
  CheckDate = false;
  CheckName = false;
  CheckType = false;

  newDateDepature : any;
  newDateArrival: any;

  constructor(private fb: FormBuilder, private pageService: PageService)
  {
    // this.tempDate = moment(this.todayDate.toLocaleString("th-TH"), 'DD-MM-YYYY');
    // this.NewDate = this.tempDate.format('YYYY-MM-DD');

    // console.log((this.NewDate));
    this.flight = new Flight("","","","",0,this.todayDate,0,0,this.todayDate);
  }

  ngOnInit(): void
  {
    this.flightForm = this.fb.group ({
      Name:     ["", [Validators.required]],
      From:     ["", [Validators.required]],
      To:       ["", [Validators.required]],
      Type:     ["", [Validators.required]],
      Adults:   ["", [Validators.required,
                      Validators.pattern('^[1-9]+')]],
      Departure:["", [Validators.required]],
      Children: ["", [Validators.required]],
      Infants:  ["", [Validators.required]],
      Arrival:  ["", [Validators.required]]
    });
    this.flightList = this.pageService.getData();
  }

  getFriendPages()
  {
    this.flightList = this.pageService.getData();
  }

  onSubmit(f:FormGroup): void
  {
    if(f.get('From')?.value != f.get('To')?.value && f.get('From')?.value != null && f.get('To')?.value != null)
    {
      this.CheckCountry = true;
      // console.log("Right Country");
    }
    else
    {
      this.CheckCountry = false;
      // console.log("Wrong Country");
    }

    if(f.get('Arrival')?.value >= f.get('Departure')?.value)
    {
      this.CheckDate = true;
      // console.log("Right Date");
    }
    else if(f.get('Arrival')?.value < f.get('Departure')?.value)
    {
      this.CheckDate = false;
      alert("Please Select Another Date!!!!");
      // console.log("Wrong Date");
    }

    if(f.get('Name')?.value != null)
    {
      this.CheckName = true;
      // console.log("Right Name");
    }
    else if(f.get('Name')?.value === null)
    {
      this.CheckName = false;
      // console.log("Wrong Name");
    }

    if(f.get('Type')?.value != null)
    {
      this.CheckType = true;
      // console.log("Right Type");
    }
    else if(f.get('Type')?.value === null)
    {
      this.CheckType = false;
      // console.log("Wrong Type");
    }

    if(this.CheckDate === true && this.CheckCountry === true && this.CheckName === true && this.CheckType === true)
    {
      // console.log(f.get('Departure')?.value.toLocaleString("th-TH"));
      // this.NewDate  = moment(f.get('Departure')?.value.toLocaleString("th-th"), 'YYYY-MM-DD');
      // this.NewDate = this.tempDate.format('DD-MM-YYYY');
      // this.NewDate = new Intl.DateTimeFormat('th-TH').format((f.get('Departure')?.value));

      this.newDateDepature = new Date((f.get('Departure')?.value)).toLocaleDateString('th-TH');

      this.newDateArrival = new Date((f.get('Arrival')?.value)).toLocaleDateString('th-TH');

      console.log(this.newDateDepature);

      let flight_data = new Flight(f.get('Name')?.value,
                                    f.get('From')?.value,
                                    f.get('To')?.value,
                                    f.get('Type')?.value,
                                    f.get('Adults')?.value,
                                    this.newDateDepature,
                                    f.get('Children')?.value,
                                    f.get('Infants')?.value,
                                    this.newDateArrival
      )
      this.pageService.addData(flight_data);
    }
  }
}
